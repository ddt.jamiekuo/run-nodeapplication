const mergeStream = require('merge-stream')

const storage = require('./storage')
const proc = require('./proc')
const env = require('./env')

const { Job } = require('@xccelerate-offsite/model')
const { Task } = require('@xccelerate-offsite/task')

const task = new Task(env.REDIS)

task.forCodeSubmit.process('Node', async (job) => {
  const jobSpec = await Job.findOne({ JobId: job.id }).exec()
  if (jobSpec === null) {
    return Promise.reject()
  }

  const { path, cleanup } = await storage.put(jobSpec.Source)

  // Create reply channel
  const forReply = task.for(job.id)

  // Spwan and execute node script
  const subproc = await proc.spawnNode(path, jobSpec.Input, 10)

  for await (const data of mergeStream(subproc.stdout, subproc.stderr)) {
    await forReply.add({ text: `${data}` })
  }
  if (subproc.killed === true) {
    await forReply.add({ text: `Task terminated Due to Timeout` })
  }

  // Disconnect reply channel
  forReply.close()

  // Remove created temporary blob object
  cleanup()
})

const { initConn } = require('@xccelerate-offsite/model')
initConn(`mongodb://${env.MONGO.HOST}:${env.MONGO.PORT}/${env.MONGO.DB}?${env.MONGO.OPTIONS}`)

console.log(`Example worker running!`)

