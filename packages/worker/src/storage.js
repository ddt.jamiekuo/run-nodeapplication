const fs = require('fs')
const tmp = require('tmp-promise')

async function put(source) {
  const { fd, path, cleanup } = await tmp.file()

  fs.writeFileSync(fd, source)
  fs.writeFileSync(fd, '\n')

  return {
    path,
    cleanup
  }
}

module.exports = { put }
