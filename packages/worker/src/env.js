module.exports = {
  IS_DEBUG: (process.env.IS_DEBUG === 'true'),

  APP_NAME: process.env.APP_NAME || 'Code_Lab',

  MONGO: {
    HOST: process.env.MONGO_HOST || 'localhost',
    PORT: parseInt(process.env.MONGO_PORT) || 27017,
    DB: process.env.MONGO_DB || 'test',
    OPTIONS: process.env.MONGO_OPTIONS || ''
  },

  REDIS: {
    HOST: process.env.REDIS_HOST || 'localhost',
    PORT: parseInt(process.env.REDIS_PORT) || 6379,
    DB: parseInt(process.env.REDIS_DB) || 0
  }
}
