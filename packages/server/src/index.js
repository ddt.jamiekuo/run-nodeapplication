const app = require('./app')
const env = require('./env')

const { initConn } = require('@xccelerate-offsite/model')
initConn(`mongodb://${env.MONGO.HOST}:${env.MONGO.PORT}/${env.MONGO.DB}?${env.MONGO.OPTIONS}`)

const port = 3333
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
