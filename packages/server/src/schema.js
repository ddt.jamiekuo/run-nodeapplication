const { validationResult: lastError } = require('express-validator')

module.exports = { lastError }
