const Queue = require('bull')

class Task {
  constructor({ host = 'localhost', port = 6379, db = 0 } = {}) {
    this.redisOpts = {
      port,
      host,
      db
    }

    this.defaultJobOptions = {
      removeOnComplete: 100,
      removeOnFail: 100
    }

    this._forCodeSubmit = null
  }

  get forCodeSubmit() {
    if (this._forCodeSubmit === null) {
      this._forCodeSubmit = new Queue('forCodeSubmit', {
        redis: this.redisOpts,
        defaultJobOptions: this.defaultJobOptions
      })
    }
    return this._forCodeSubmit
  }

  for(name, { redis = this.redisOpts, defaultJobOptions = this.defaultJobOptions } = {}) {
    return new Queue(name, { redis, defaultJobOptions })
  }
}

module.exports = { Task }
